<?php

use Illuminate\Database\Seeder;

class CommentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       	DB::table('comments')->insert([
			[
				'post_id' => 1,
	            'author' => 'Vasya',
	            'content' => 'My first comment',
	            
	        ],
			[
				'post_id' => 1,
	            'author' => 'Petya',
	            'content' => 'My second comment',
	            
	        ],
	        	[
				'post_id' => 2,
	            'author' => 'Kolya',
	            'content' => 'My first comment second post',
	            
	        ],
			[
				'post_id' => 2,
	            'author' => 'Sasha',
	            'content' => 'My second comment second post',
	            
	        ]
	    ]);

    }
}
