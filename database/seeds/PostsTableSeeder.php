<?php

use Illuminate\Database\Seeder;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('posts')->insert([
            [
            'author' => 'admin',
            'name' => 'my first post',
            'content' => 'Hello World!',
            
        ],
        [
            'author' => 'admin',
            'name' => 'my second post',
            'content' => 'Hi!',
            
        ]]
    );
    }
}
