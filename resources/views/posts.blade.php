<!DOCTYPE html>
<html>
<head>
	<title>Posts</title>
</head>
<body>
	<h1>Posts</h1>
	@foreach ($posts as $post)
		<h2>{{$post->name}}</h2>
		<div class ="author">{{ $post->author }}</div>
		<p>{{$post->content}}</p>
		 <a href="/posts/{{ $post->id }}">more...</a>
		@foreach ($post->comments as $comment)
			<div class ="author-comment">{{ $comment->author }}</div>
			<div>{{ $comment->content }}</div>
			@endforeach
	@endforeach
</body>
</html>